
	/**
 * Classe Hexagono
 * 
 * @author Matheus Le�o
 *
 */
public class Hexagono extends Figura {
	/**
	 * Construtor da classe Hexagono.
	 * 
	 * @param x
	 *            Valor do x inicial
	 * @param y
	 *            Valor do y inicial
	 * @param lado
	 *            Tamanho do lado
	 */
	public Hexagono(double y, double x, double lado){
		super(6, lado);
		criaVertices(x, y);
		criaArestas();
		calculaCentro();
	}
	
	/**
	 * M�todo para Criar o conjunto de v�rtices da figura.
	 * 
	 * @param x
	 *            Valor x inicial.
	 * @param y
	 *            Valor y inicial.
	 */
	
	public void criaVertices(double x, double y){
		double tempX;
		double tempY;
		
		tempX = x;
		tempY = y;
		
		vertices[0] = new Vertice(tempX,tempY);
		
		tempX = x + lado;
		vertices[1] = new Vertice(tempX, y);
		
		tempX = (x)+(lado * 1.5);
		tempY = (y) + (lado * 0.866);
		vertices[2] = new Vertice(tempX, tempY);
		
		tempX = x + lado;
		tempY = ((y) + (lado * 1.732));
		vertices[3] = new Vertice(tempX, tempY);
		
		tempX = x;
		tempY = (y) + (lado * 1.732);
		vertices[4] = new Vertice(tempX, tempY);
		
		tempX = x + ((lado * 1.5) - (2 *lado));
		tempY = (y) + (lado * 0.866);
		vertices[5] = new Vertice(tempX, tempY);
	}

	/**
	 * Calcula a ap�tema do Hex�gono.
	 * 
	 * @return Ap�tema do Hex�gono.
	 */
	public double calculaApotema(){
		double area = calculaArea();
		double perimetro = calculaPerimetro();
		double apotema = (area*2)/perimetro;
		return apotema;
	}
	/**
	 * Calcula o centro do hexagono.
	 * 
	 */
	@Override 
	public void calculaCentro() {
		double apotema = calculaApotema();
		double x1 = vertices[0].getX();
		double x2 = vertices[1].getX();
		double xCentro = (x1 + x2) / 2;
		double yCentro = vertices[0].getY() + apotema;
		Vertice centro = new Vertice(xCentro, yCentro);
		this.centro = centro;
	}

	/**
	 * Calcula a �rea do Hex�gono
	 * 
	 * @return �rea do Hexagono
	 */
	@Override
	
	public double calculaArea() {
			return (((3 * Math.sqrt(3)) * (lado * lado)) / 2);
	}
	/**
	 * Calcula o per�metro do Hex�gono.
	 * 
	 * @return Per�metro da Hexagono.
	 */
	@Override
	
	public double calculaPerimetro() {
		return 6*lado;
	}
}

/**
 * Classe Retangulo
 * 
 * @author Eric
 *
 */
public class Retangulo extends Figura {
	private double altura;

	/**
	 * Construtor da classe Retangulo.
	 * 
	 * @param x
	 *            Valor do x inicial.
	 * @param y
	 *            Valor do y inicial.
	 * @param lado
	 *            Tamanho do lado(base) do rent�ngulo.
	 * @param altura
	 *            Tamanho da altura do ret�ngulo.
	 */
	public Retangulo(double x, double y, double lado, double altura) {
		super(4, lado);
		this.altura = altura;
		criaVertices(x, y);
		criaArestas();
		calculaCentro();
	}

	/**
	 * Calcula a �rea da figura
	 * 
	 * @return �rea da figura.
	 */
	@Override
	public double calculaArea() {
		return lado * altura;
	}

	/**
	 * Calcula o per�metro da figura.
	 * 
	 * @return Per�metro da figura.
	 */
	@Override
	public double calculaPerimetro() {
		return lado * 2 + altura * 2;
	}

	/**
	 * Cria o conjunto de v�rtices da figura.
	 * 
	 * @param x
	 *            Valor x inicial.
	 * @param y
	 *            Valor y inicial.
	 */
	@Override
	public void criaVertices(double x, double y) {
		double tempX, tempY;
		vertices[0] = new Vertice(x, y);
		tempX = vertices[0].getX() + lado;
		tempY = vertices[0].getY();
		vertices[1] = new Vertice(tempX, tempY);
		tempX = vertices[1].getX();
		tempY = vertices[1].getY() + altura;
		vertices[2] = new Vertice(tempX, tempY);
		tempX = vertices[2].getX() - lado;
		tempY = vertices[2].getY();
		vertices[3] = new Vertice(tempX, tempY);
	}

	/**
	 * Calcula o centro da figura.
	 * 
	 */
	@Override
	public void calculaCentro() {
		double xCentro = (vertices[0].getX() + vertices[1].getX()) / 2;
		double yCentro = (vertices[1].getY() + vertices[2].getY()) / 2;
		this.centro = new Vertice(xCentro, yCentro);
	}

	/**
	 * Altera o tamanho da figura proporcionalmente. Para diminuir o tamanho,
	 * informar valor negativo.
	 * 
	 * @param valor
	 *            Valor da porcentagem em que a figura deve ser aumentada ou
	 *            diminu�da.
	 */
	@Override
	public void alteraTamanho(double valor) {
		lado = (valor / 100) * lado + lado;
		altura = (valor / 100) * altura + altura;
		if (this.grau == 0.0) {
			criaVertices(vertices[0].getX(), vertices[0].getY());
			criaArestas();
			calculaCentro();
		} else {
			double tempGrau = this.grau;
			tempGrau *= -1;
			rotacao(tempGrau);
			criaVertices(vertices[0].getX(), vertices[0].getY());
			calculaCentro();
			tempGrau *= -1;
			rotacao(tempGrau);
			criaArestas();
		}

	}

}
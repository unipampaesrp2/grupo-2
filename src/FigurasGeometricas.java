import java.util.Scanner;

/**
 * Classe principal FigurasGeom�tricas.
 * 
 * @author Diovane Freitas, Eric Oliveira, Matheus Le�o, Ricardo Gentile
 *
 */
public class FigurasGeometricas {
	static Scanner ler = new Scanner(System.in);
	static IQuadrado quadrado = new IQuadrado();
	static IPentagono pentagono = new IPentagono();
	static IRetangulo retangulo = new IRetangulo();
	static IHexagono hexagono = new IHexagono();
	static ITriangulo triangulo = new ITriangulo();

	/**
	 * M�todo principal do projeto das figuras geom�tricas.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		int op;
		boolean sair = false;
		do {
			System.out.println("Figuras Geom�tricas");
			System.out.println("Menu Principal\n" + "1 - Tri�ngulo Regular\n"
					+ "2 - Quadrado\n" + "3 - Ret�ngulo\n"
					+ "4 - Pent�gono Regular\n" + "5 - Hex�gono Regular\n"
					+ "6 - Sair");
			do {
				op = ler.nextInt();
				ler.nextLine();
				if (op < 1 || op > 6) {
					System.out.println("Op��o inv�lida. (Op��es de 1 a 6)");
				}
			} while (op < 1 || op > 6);
			switch (op) {
			case 1:
				triangulo.menu();
				break;
			case 2:
				quadrado.menu();
				break;
			case 3:
				retangulo.menu();
				break;
			case 4:
				pentagono.menu();
				break;
			case 5:
				hexagono.menu();
				break;
			case 6:
				sair = sair();
				break;
			}
		} while (!sair);
		System.out.println("Programa Finalizado.");
	}

	/**
	 * Verifica se o usu�rio deseja mesmo sair do programa.
	 * 
	 * @return Verdadeiro se o usu�rio deseja sair, e falso caso contr�rio.
	 */
	public static boolean sair() {
		String op;
		do {
			System.out.println("Deseja realmente sair? (S/N)");
			op = ler.nextLine();
			if (!op.equalsIgnoreCase("s") && !op.equalsIgnoreCase("n")) {
				System.out
						.println("Op��o inv�lida. Digite S para SIM e N para N�O.");
			}
		} while (!op.equalsIgnoreCase("s") && !op.equalsIgnoreCase("n"));
		if (op.equalsIgnoreCase("s")) {
			return true;
		} else {
			return false;
		}
	}

}

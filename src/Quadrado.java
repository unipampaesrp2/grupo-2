
public class Quadrado extends Figura {
	//Construtor, onde utiliza os pontos x e y iniciais e a medida do lado.
	public Quadrado(double x, double y, double lado) {
		super(4, lado );
		criaVertices(x, y);
		criaArestas();
		calculaCentro();
		
	}

	@Override
	//M�todo do c�lculo da �rea, calculando o lado�.
	public double calculaArea() {
		
		return lado*lado;
	}

	@Override
	//M�todo do c�lculo do per�metro, calculando o lado*4.
	public double calculaPerimetro() {
		
		return lado*4;
	}

	@Override
	
	/**M�todo que calcula os vertices,
	 *  utilizando o valor do lado que, dependendo do caso,
	 *   soma com os valores x e y iniciais.
	 */
	public void criaVertices(double x, double y) {
		vertices[0]=new Vertice(x,y);
		vertices[1]=new Vertice(x + lado, y);
		vertices[3]=new Vertice(x,y + lado);
		vertices[2]=new Vertice(x+lado,y+lado);		
	}

	@Override
	/**M�todo que calcula o centro da figura,
	 * utilizando a metade do valor x de uma aresta horizontal
	 * e a metade do y de uma aresta vertical.
	 */
	public void calculaCentro() {
		double centrox;
		double centroy;
		centrox = (vertices[0].getX()+vertices[1].getX())/2;
		centroy = (vertices[0].getY()+vertices[2].getY())/2;
		Vertice centro = new Vertice(centrox, centroy);
		this.centro=centro;
	}
	
}

/**
 * Classe Vertice
 * 
 * @author Diovane Freitas, Eric Oliveira, Matheus Le�o, Ricardo Gentile
 *
 */
public class Vertice {

	private double x, y;

	/**
	 * Construtor da classe Vertice
	 * 
	 * @param x
	 *            Valor de x
	 * @param y
	 *            Valor de y
	 */
	public Vertice(double x, double y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Retorna o valor de x.
	 * 
	 * @return Valor de x.
	 */
	public double getX() {
		return x;
	}

	/**
	 * Atribui um valor a x.
	 * 
	 * @param x
	 *            Valor de x.
	 */
	public void setX(double x) {
		this.x = x;
	}

	/**
	 * Retorna o valor de y.
	 * 
	 * @return Valor de y.
	 */
	public double getY() {
		return y;
	}

	/**
	 * Atribui um valor a y.
	 * 
	 * @param y
	 *            Valor de y.
	 */
	public void setY(double y) {
		this.y = y;
	}

}

import java.awt.Color;

public class Triangulo extends Figura {

	/*
	 * Construtor recebe aprimeira vertice e a quantidade de lados para calcular
	 * o restante das informa�oes
	 */
	public Triangulo(double x, double y, double lado) {

		super(3, lado);
		criaVertices(x, y);
		criaArestas();
		calculaCentro();

	}

	// Somando os Lados do Triangulo obt�m-se o perimetro da figura
	public double calculaPerimetro() {

		double perimetro = 3 * getLado();
		return perimetro;
	}

	@Override
	public double calculaArea() {
		double area = getLado() * getLado() / 2;

		return area;
	}

	@Override
	public void criaVertices(double x, double y) {

		/*
		 * Informando primeiros valores para formar a primeira vertice, � gerado
		 * as outras duas vertices do triangulo
		 */
		double tempX = x + lado;
		double tempY = y;

		vertices[0] = new Vertice(x, y);
		vertices[1] = new Vertice(tempX, tempY);
		tempX = (vertices[0].getX() + vertices[1].getX()) / 2;
		tempY = y + this.calculaAltura();
		vertices[2] = new Vertice(tempX, tempY);

	}

	public double calculaAltura() {
		/*
		 * Calcula a Altura do Triangulo Mediante a entrada da base e uma
		 * coordenada xy!
		 */

		double altura = (this.getLado() * Math.sqrt(3)) / 2;

		return altura;

	}

	public void calculaCentro() {

		/*
		 * A divisao por dois da soma entre as primeiras abscissas x e a divisao
		 * por dois da soma da ordenada y com o a altura, geramos a coordenada
		 * central figura .
		 */

		double altura = calculaAltura();
		double centroX = (vertices[0].getX() + vertices[1].getX()) / 2;
		double centroY = vertices[0].getY() + (altura / 2);
		Vertice centro = new Vertice(centroX, centroY);
		this.centro = centro;

	}

}

    
	import java.awt.Color;
import java.util.ArrayList;
import java.util.Scanner;
/**
 * Interface do Hexagono.
 * 
 * @author Matheus Le�o
 *
 */
	public class IHexagono {
		private ArrayList<Hexagono> figuras = new ArrayList<Hexagono>();
		private Scanner ler = new Scanner(System.in);
		
		/*
		 * M�todo Construtor que inicializa j� com 3 objetos criados
		 * 
		 */
		public IHexagono(){
			cria3();
		}

		/**
		 * Menu com Interface do Hex�gono
		 * 
		 */
		public void menu() {
			int op;
			do {
				System.out.println("Menu Hex�gono");
				System.out.println("1 - Criar Novo\n"
								 + "2 - Exibir\n"
								 + "3 - Altera��es\n" 
								 + "4 - Excluir\n" 
								 + "5 - Voltar");
				
				do {
					op = ler.nextInt();
					if (op < 1 || op > 5) {
						System.out.println("Op��o inv�lida. (Op��es de 1 a 5)");
					}
				} while (op < 1 || op > 5);
				switch (op) {
				case 1:
					criar();
					break;
				case 2:
					exibir();
					break;
				case 3:
					alterar();
					break;
				case 4:
					excluir();
					break;
				}
			} while (op != 5);
		}

		/**
		 * M�todo para criar um novo hex�gono
		 * 
		 */
		private void criar() {
			System.out.print("Informe as coordenadas x e y iniciais: ");
			double x = ler.nextDouble();
			double y = ler.nextDouble();
			System.out.print("Informe o tamanho do lado da figura: ");
			double lado = ler.nextDouble();
			Hexagono novo = new Hexagono(x, y, lado);
			System.out.println("Escolha a cor da borda:");
			novo.setCorBorda(menuCores());
			System.out.println("Escolha a cor do preenchimento:");
			novo.setCorPreenchimento(menuCores());
			figuras.add(novo);
		}
		/**
		 * M�todo para exibir os Hex�gonos j� criados
		 * com op��o de exibir todos ou exibir um Hex�gono espec�fico escolhido pelo usu�rio
		 */

		private void exibir() {
			int op;
			System.out.println("1 - Exibir Todos\n" + "2 - Exibir Figura Espec�fica\n"
					+ "3 - Voltar");
			do {
				op = ler.nextInt();
				if (op < 1 || op > 3) {
					System.out.println("Op��o inv�lida. (Op��es de 1 a 3)");
				}
			} while (op < 1 || op > 3);
			switch (op) {
			case 1:
				for (int i = 0; i < figuras.size(); i++) {
					System.out.println(" - Hex�gono " + (i + 1) + " - ");
					System.out.println(figuras.get(i).strCompleto());
				}
				break;
			case 2:
				int indice;
				System.out.println("Hex�gonos Dispon�veis");
				for (int i = 0; i < figuras.size(); i++) {
					System.out.println("Hex�gono " + (i + 1));
				}
				do {
					System.out.println("Qual Hex�gono a ser alterado?");
					indice = ler.nextInt();
					if (indice < 1 || indice > figuras.size()) {
						System.out.println("Op��o inv�lida.");
					}
				} while (indice < 1 || indice > figuras.size());
				indice--;
				System.out.println(" - Figura " + (indice + 1) + " - ");
				System.out.println(figuras.get(indice).strCompleto());
				break;
			case 3:
				break;
			}
		}
		
		/**
		 * M�todo para alterar Hexagonos j� existentes
		 * 
		 */

		private void alterar() {
			if (figuras.size() > 0) {
				int indice;
				int opcao;
				System.out.println("Hex�gonos Dispon�veis");
				for (int i = 0; i < figuras.size(); i++) {
					System.out.println("Figura " + (i + 1));
				}
				do {
					System.out.println("Qual o Hex�gono a ser alterado?");
					indice = ler.nextInt();
					if (indice < 1 || indice > figuras.size()) {
						System.out.println("Op��o inv�lida.");
					}
				} while (indice < 1 || indice > figuras.size());
				indice--;
				System.out.println("1 - Transla��o\n" 
								 + "2 - Rota��o\n"
								 + "3 - Tamanho\n" 
								 + "4 - Cor da Borda\n"
								 + "5 - Cor do Preenchimento\n");
				do {
					System.out.print("Escolha uma op��o: ");
					opcao = ler.nextInt();
					ler.nextLine();
					if (opcao < 1 || opcao > 5) {
						System.out.println("Op��o inv�lida.\n");
					}
				} while (opcao < 1 || opcao > 5);
				System.out.println("\n");
				switch (opcao) {
				case 1:
					double x,
					y;
					System.out.println("Informe a transla��o no eixo X:");
					x = ler.nextDouble();
					System.out.println("Informe a transla��o no eixo Y:");
					y = ler.nextDouble();
					figuras.get(indice).translacao(x, y);
					System.out.println("Sucesso na transla��o.");
					break;
				case 2:
					double graus;
					System.out.println("Informe quantos graus deseja rotacionar:");
					graus = ler.nextDouble();
					figuras.get(indice).rotacao(graus);
					System.out.println("Sucesso na rota��o.");
					break;
				case 3:
					double porcento;
					System.out
							.println("Informe quantos por cento deseja aumentar ou diminuir:");
					porcento = ler.nextDouble();
					figuras.get(indice).alteraTamanho(porcento);
					System.out.println("Sucesso na redimens�o.");
					break;
				case 4:
					figuras.get(indice).setCorBorda(menuCores());
					System.out.println("Cor da borda alterada.");
					break;
				case 5:
					figuras.get(indice).setCorPreenchimento(menuCores());
					System.out.println("Cor do preenchimento alterada.");
					break;
				}
			} else {
				System.out.println("Ainda n�o h� figuras adicionadas.\n");
			}
		}

		private void excluir() {
			if (figuras.size() > 0) {
				int indice;
				System.out.println("Hex�gonos Dispon�veis");
				for (int i = 0; i < figuras.size(); i++) {
					System.out.println("Hex�gono " + (i + 1));
				}
				do {
					System.out.println("Qual o Hex�gono a ser exclu�do ?");
					indice = ler.nextInt();
					if (indice < 1 || indice > figuras.size()) {
						System.out.println("Op��o inv�lida.");
					}
				} while (indice < 1 || indice > figuras.size());
				indice--;
				figuras.remove(indice);
				System.out.println("Removido com sucesso.");

			} else {
				System.out.println("Ainda n�o h� figuras adicionadas.\n");
			}
		}
		
		/**
		 * Menu para escolher as cores da borda e Preenchimento j� predefinidas com uso da Classe Color
		 * @return Cor selecionada.
		 */
		
		private Color menuCores() {
			int indice;
			String cores[] = { "Preto", "Azul", "Vermelho", "Rosa", "Amarelo",
					"Verde" };
			Color colors[] = { Color.black, Color.blue, Color.red, Color.pink,
					Color.yellow, Color.green };
			for (int i = 0; i < cores.length; i++) {
				System.out.println((i + 1) + " - " + cores[i]);
			}
			do {
				System.out.print("Escolha uma das op��es: ");
				indice = ler.nextInt();
				if (indice < 1 || indice > cores.length) {
					System.out.println("Op��o inv�lida.");
				}
			} while (indice < 1 || indice > cores.length);
			return colors[indice - 1];
		}
			private void cria3() {
				Hexagono n1 = new Hexagono(1, 1, 3);
				n1.setCorBorda(Color.PINK);
				n1.setCorPreenchimento(Color.RED);
				figuras.add(n1);
				Hexagono n2 = new Hexagono(3, 1, 4);
				n2.setCorBorda(Color.blue);
				n2.setCorPreenchimento(Color.green);
				figuras.add(n2);
				Hexagono n3 = new Hexagono(2, 4, 5);
				n3.setCorBorda(Color.green);
				n3.setCorPreenchimento(Color.yellow);
				figuras.add(n3);
			}
		}

import java.awt.Color;
import java.util.ArrayList;
import java.util.Scanner;

public class IQuadrado {
	private ArrayList<Quadrado> figuras = new ArrayList<Quadrado>();
	private Scanner ler = new Scanner(System.in);
	
	//Invoca o m�todo cria3, que cria automaticamente 3 figuras.
	public IQuadrado(){
		cria3();
	}
	//M�todo do menu da figura quadrado.
	public void menu() {
		int op;
		
		do {
			//Menu, onde cada numero representa uma funcionalidade diferente.
			System.out.println("Menu Quadrado");
			System.out.println("1 - Criar Novo\n" + "2 - Exibir\n"
					+ "3 - Altera��es\n" + "4 - Excluir\n" + "5 - Voltar");
			do {
				//Valida��o para que o usu�rio n�o digite uma op��o que n�o exista.
				op = ler.nextInt();
				if (op < 1 || op > 5) {
					System.out.println("Op��o inv�lida. (Op��es de 1 a 5)");
				}
			} while (op < 1 || op > 5);
			switch (op) {
			//Invoca os m�todos dos respectivos numeros do menu.
			case 1:
				criar();
				break;
			case 2:
				exibir();
				break;
			case 3:
				alterar();
				break;
			case 4:
				excluir();
				break;
			}
		} while (op != 5);
	}
	//M�todo que permite criar uma nova figura, recebendo os dados do usu�rio.
	private void criar() {
		System.out.print("Informe as coordenadas x e y iniciais: ");
		double x = ler.nextDouble();
		double y = ler.nextDouble();
		System.out.print("Informe o tamanho do lado da figura: ");
		double lado = ler.nextDouble();
		Quadrado novo = new Quadrado(x, y, lado);
		System.out.println("Escolha a cor da borda:");
		novo.setCorBorda(menuCores());
		System.out.println("Escolha a cor do preenchimento:");
		novo.setCorPreenchimento(menuCores());
		figuras.add(novo);
	}
	//M�todo utilizado para a exibi��o dos dados da figura que o usu�rio deseja ver.
	private void exibir() {
		int op;
		System.out.println("1 - Exibir Todos\n" + "2 - Exibir Quadrado Espec�fico\n"
				+ "3 - Voltar");
		do {
			//Valida��o para que o usu�rio n�o digite uma op��o que n�o exista.
			op = ler.nextInt();
			if (op < 1 || op > 3) {
				System.out.println("Op��o inv�lida. (Op��es de 1 a 3)");
			}
		} while (op < 1 || op > 3);
		//Switch onde realiza a op��o que o usu�rio digitou anteriormente.
		switch (op) {
		case 1:
			//Ser� exibido as informa��es de todas as figuras existentes.
			for (int i = 0; i < figuras.size(); i++) {
				System.out.println(" - Figura " + (i + 1) + " - ");
				System.out.println(figuras.get(i).strCompleto());
			}
			break;
		case 2:
			//Ser� exibido o n�mero de figuras existentes.
			int indice;
			System.out.println("Quadrados Dispon�veis");
			for (int i = 0; i < figuras.size(); i++) {
				System.out.println("Quadrado " + (i + 1));
			}
			do {
				//Pede ao usuario a figura que ele deseja ver as informa��es.
				System.out.println("Qual o quadrado a ser alterado?");
				indice = ler.nextInt();
				//Valida��o para que o usu�rio n�o digite uma op��o que n�o exista.
				if (indice < 1 || indice > figuras.size()) {
					System.out.println("Op��o inv�lida.");
				}
			} while (indice < 1 || indice > figuras.size());
			indice--;
			//Exibe os dados da figura selecionada pelo usu�rio.
			System.out.println(" - Quadrado " + (indice + 1) + " - ");
			System.out.println(figuras.get(indice).strCompleto());
			break;
		case 3:
			break;
		}
	}
	//M�todo para alterar os dados de alguma figura.
	private void alterar() {
		if (figuras.size() > 0) {
			int indice;
			int opcao;
			//Exibe o numero de figuras disponiveis.
			System.out.println("Quadrados Dispon�veis");
			for (int i = 0; i < figuras.size(); i++) {
				System.out.println("Quadrado " + (i + 1));
			}
			do {
				//Pede ao usuario qual figura ele deseja alterar.
				System.out.println("Qual o quadrado a ser alterado?");
				indice = ler.nextInt();
				//Valida��o para que o usu�rio n�o digite uma op��o que n�o exista.
				if (indice < 1 || indice > figuras.size()) {
					System.out.println("Op��o inv�lida.");
				}
			} while (indice < 1 || indice > figuras.size());
			indice--;
			//Menu referente as altera��es que o usu�rio deseja fazer
			System.out.println("1 - Transla��o\n" + "2 - Rota��o\n"
					+ "3 - Tamanho\n" + "4 - Cor da Borda\n"
					+ "5 - Cor do Preenchimento\n");
			do {
				System.out.print("Escolha uma op��o: ");
				opcao = ler.nextInt();
				ler.nextLine();
				//Valida��o para que o usu�rio n�o digite uma op��o que n�o exista.
				if (opcao < 1 || opcao > 5) {
					System.out.println("Op��o inv�lida.\n");
				}
			} while (opcao < 1 || opcao > 5);
			System.out.println("\n");
			//Switch para selecionar a op��o que o usu�rio informou anteriormente.
			switch (opcao) {
			case 1:
				double x,y;
				//Pede os valores que o usu�rio deseja mover no eixo x e y da figura. 
				System.out.println("Informe a transla��o no eixo X:");
				x = ler.nextDouble();
				System.out.println("Informe a transla��o no eixo Y:");
				y = ler.nextDouble();
				figuras.get(indice).translacao(x, y);
				System.out.println("Sucesso na transla��o.");
				break;
			case 2:
				double graus;
				//Pede ao usu�rio quantos graus ele deseja rotacionar a figura.
				System.out.println("Informe quantos graus deseja rotacionar:");
				graus = ler.nextDouble();
				figuras.get(indice).rotacao(graus);
				System.out.println("Sucesso na rota��o.");
				break;
			case 3:
				double porcento;
				//Pede ao usu�rio o quanto ele deseja aumentar a figura em porcentagem.
				System.out.println("Informe a porcentagem que deseja aumentar ou diminuir:");
				porcento = ler.nextDouble();
				figuras.get(indice).alteraTamanho(porcento);
				System.out.println("Sucesso na redimens�o.");
				break;
			case 4:
				//Invoca o menuCores, que exibe as cores dispon�veis para o usu�rio alterar a cor da borda da figura.
				figuras.get(indice).setCorBorda(menuCores());
				System.out.println("Cor da borda alterada.");
				break;
			case 5:
				//Invoca o menuCores, que exibe as cores dispon�veis para o usu�rio alterar a cor do preenchimento da figura.
				figuras.get(indice).setCorPreenchimento(menuCores());
				System.out.println("Cor do preenchimento alterada.");
				break;
			}
		} else {
			//Exibe um erro se n�o existir alguma figura salva.
			System.out.println("N�o h� quadrados adicionados.\n");
		}
	}
	
	//M�todo para excluir figuras.
	private void excluir() {
		if (figuras.size() > 0) {
			int indice;
			//Exibe as figuras dispon�veis.
			System.out.println("Quadrados Dispon�veis");
			for (int i = 0; i < figuras.size(); i++) {
				System.out.println("Quadrado " + (i + 1));
			}
			do {
				//Pede ao usu�rio, informar qual figura ele deseja remover
				System.out.println("Qual o quadrado a ser removido?");
				indice = ler.nextInt();
				//Valida��o para que o usu�rio n�o digite uma op��o que n�o exista.
				if (indice < 1 || indice > figuras.size()) {
					System.out.println("Op��o inv�lida.");
				}
			} while (indice < 1 || indice > figuras.size());
			indice--;
			//Remove a figura que o usu�rio informou.
			figuras.remove(indice);
			System.out.println("Removido com sucesso.");

		} else {
			//Exibe um erro se n�o existir alguma figura salva.
			System.out.println("Ainda n�o h� quadrados adicionados.\n");
		}
	}
	
	//M�todo para um menu de cores dispon�veis para o usu�rio editar.
	private Color menuCores() {
		int indice;
		//Uma esp�cie de menu que exibe as cores dispon�veis.
		String cores[] = { "Preto", "Azul", "Vermelho", "Rosa", "Amarelo",
				"Verde" };
		Color colors[] = { Color.black, Color.blue, Color.red, Color.pink,
				Color.yellow, Color.green };
		for (int i = 0; i < cores.length; i++) {
			//Exibe o menu de cores.
			System.out.println((i + 1) + " - " + cores[i]);
		}
		do {
			//Pede ao usu�rio para que escolha alguma das cores.
			System.out.print("Escolha uma das op��es: ");
			indice = ler.nextInt();
			if (indice < 1 || indice > cores.length) {
				//Valida��o para que o usu�rio n�o digite uma op��o que n�o exista.
				System.out.println("Op��o inv�lida.");
			}
		} while (indice < 1 || indice > cores.length);
		return colors[indice - 1];
	}
	
	//M�todo que cria autom�ticamente 3 figuras.
	private void cria3(){
		//Cria a figura n�mero 1, com os valores x e y iniciais a medida do lado.
		Quadrado n1 = new Quadrado(1, 1, 3);
		//Define a cor da borda como "Vermelho" e a cor do preenchimento como "Preto".
		n1.setCorBorda(Color.RED);
		n1.setCorPreenchimento(Color.BLACK);
		figuras.add(n1);
		//Cria a figura n�mero 2, com os valores x e y iniciais a medida do lado.
		Quadrado n2 = new Quadrado(1, 5, 2);
		//Define a cor da borda como "Azul" e a cor do preenchimento como "Amarelo".
		n2.setCorBorda(Color.blue);
		n2.setCorPreenchimento(Color.yellow);
		figuras.add(n2);
		//Cria a figura n�mero 3, com os valores x e y iniciais a medida do lado.
		Quadrado n3 = new Quadrado(3, 2, 4);
		//Define a cor da borda como "Verde" e a cor do preenchimento como "Amarelo".
		n3.setCorBorda(Color.green);
		n3.setCorPreenchimento(Color.yellow);
		figuras.add(n3);
	}
}
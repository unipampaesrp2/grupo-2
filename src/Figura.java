import java.awt.Color;
import java.text.DecimalFormat;

/**
 * Classe Figura
 * 
 * @author Diovane Freitas, Eric Oliveira, Matheus Le�o, Ricardo Gentile
 *
 */
public abstract class Figura {

	private int nLados;
	protected double lado;
	protected Vertice vertices[];
	protected Aresta arestas[];
	protected Vertice centro;
	protected double grau;
	private Color corBorda;
	private Color corPreenchimento;

	/**
	 * Construtor da classe Figura.
	 * 
	 * @param nLados
	 *            Quantidade de lados da figura.
	 * @param lado
	 *            Tamanho do lado.
	 */
	public Figura(int nLados, double lado) {
		setnLados(nLados);
		this.lado = lado;
		this.grau = 0;
	}

	/**
	 * Retorna a quantidade de lados da figura.
	 * 
	 * @return Quantidade de lados da figura.
	 */
	public int getnLados() {
		return nLados;
	}

	/**
	 * Atribui o n�mero de lados da figura e inicializa os vetores de v�rtices e
	 * arestas de acordo com o seu tamanho.
	 * 
	 * @param nLados
	 *            Quantidade de lados da figura.
	 */
	public void setnLados(int nLados) {
		this.nLados = nLados;
		vertices = new Vertice[nLados];
		arestas = new Aresta[nLados];
	}

	/**
	 * Retorna o tamanho de um dos lados da figura.
	 * 
	 * @return Tamanho de um dos lados da figura..
	 */
	public double getLado() {
		return lado;
	}

	/**
	 * Atribui o tamanho do lado da figura.
	 * 
	 * @param lado
	 *            Tamanho do lado da figura.
	 */
	public void setLado(double lado) {
		this.lado = lado;
	}

	/**
	 * Retorna o v�rtice com as coordenadas do ponto central da figura.
	 * 
	 * @return V�rtice com as coordenadas do ponto central da figura.
	 */
	public Vertice getCentro() {
		return centro;
	}

	/**
	 * Atribui o ponto central da figura.
	 * 
	 * @param centro
	 *            V�rtice com as coordenadas do ponto central da figura.
	 */
	public void setCentro(Vertice centro) {
		this.centro = centro;
	}

	/**
	 * Retorna o conjunto de v�rtices.
	 * 
	 * @return Conjunto de v�rtices.
	 */
	public Vertice[] getVertices() {
		return vertices;
	}

	/**
	 * Atribui um v�rtice � determinada posi��o do vetor de v�rtices.
	 * 
	 * @param i
	 *            Posi��o do v�rtice.
	 * @param x
	 *            Coordenada x.
	 * @param y
	 *            Coordenada y.
	 */
	public void setVertice(int i, double x, double y) {
		vertices[i] = new Vertice(x, y);
	}

	/**
	 * Retorna o conjunto de arestas.
	 * 
	 * @return Conjunto de arestas.
	 */
	public Aresta[] getArestas() {
		return arestas;
	}

	/**
	 * Cria o conjunto de arestas.
	 * 
	 */
	public void criaArestas() {
		for (int i = 0; i < vertices.length; i++) {
			if (i < vertices.length - 1) {
				arestas[i] = new Aresta(vertices[i], vertices[i + 1]);
			} else {
				arestas[i] = new Aresta(vertices[i], vertices[0]);
			}
		}

	}

	/**
	 * Retorna a cor da borda.
	 * 
	 * @return Cor da borda.
	 */
	public Color getCorBorda() {
		return corBorda;
	}

	/**
	 * Atribui a cor da borda.
	 * 
	 * @param corBorda
	 *            Cor da borda.
	 */
	public void setCorBorda(Color corBorda) {
		this.corBorda = corBorda;
	}

	/**
	 * Retorna a cor do preenchimento.
	 * 
	 * @return Cor do preenchimento.
	 */
	public Color getCorPreenchimento() {
		return corPreenchimento;
	}

	/**
	 * Atribui a cor do preenchimento.
	 * 
	 * @param corPreenchimento
	 *            Cor do preenchimento.
	 */
	public void setCorPreenchimento(Color corPreenchimento) {
		this.corPreenchimento = corPreenchimento;
	}

	/**
	 * Retorna a quantidade graus a figura j� foi rotacionada.
	 * 
	 * @return Quantidade de graus que a figura j� foi rotacionada.
	 */
	public double getGrau() {
		return grau;
	}

	/**
	 * Atribui a quantidade de graus que a figura foi rotacionada.
	 * 
	 * @param grau
	 *            Quantidade de graus que a figura foi rotacionada.
	 */
	public void setGrau(double grau) {
		this.grau = grau;
	}

	/**
	 * Faz a transla��o da figura. Para transla��o para a direita ou para baixo,
	 * informar valores negativos.
	 * 
	 * @param x
	 *            Quantidade de unidades em que a figura deve ser rotacionada no
	 *            eixo x.
	 * @param y
	 *            Quantidade de unidades em que a figura deve ser rotacionada no
	 *            eixo y.
	 */
	public void translacao(double x, double y) {
		for (int i = 0; i < vertices.length; i++) {
			double xTemp = vertices[i].getX();
			double yTemp = vertices[i].getY();
			xTemp += x;
			yTemp += y;
			vertices[i].setX(xTemp);
			vertices[i].setY(yTemp);
		}
		centro.setX(centro.getX() + x);
		centro.setY(centro.getY() + y);
	}

	/**
	 * Faz a rota��o da figura. Para rota��o no sentido hor�rio, informar
	 * valores negativos.
	 * 
	 * @param graus
	 *            Quantidade de graus que a figura deve ser rotacionada.
	 */
	public void rotacao(double graus) {
		double tempGrau = graus;
		graus *= Math.PI / 180;
		double tX = centro.getX() * -1;
		double tY = centro.getY() * -1;
		translacao(tX, tY);
		for (int i = 0; i < vertices.length; i++) {
			double x = vertices[i].getX();
			double y = vertices[i].getY();
			double xTemp = x * Math.cos(graus) - y * Math.sin(graus);
			double yTemp = x * Math.sin(graus) + y * Math.cos(graus);
			setVertice(i, xTemp, yTemp);
		}
		tX *= -1;
		tY *= -1;
		translacao(tX, tY);
		criaArestas();
		tempGrau += getGrau();
		setGrau(tempGrau);
	}

	/**
	 * Altera o tamanho da figura proporcionalmente. Para diminuir o tamanho,
	 * informar valor negativo.
	 * 
	 * @param valor
	 *            Valor da porcentagem em que a figura deve ser aumentada ou
	 *            diminu�da.
	 */
	public void alteraTamanho(double valor) {
		valor /= 100;
		valor *= lado;
		lado += valor;
		if (this.grau == 0.0) {
			criaVertices(vertices[0].getX(), vertices[0].getY());
			criaArestas();
			calculaCentro();
		} else {
			double tempGrau = this.grau;
			tempGrau *= -1;
			rotacao(tempGrau);
			criaVertices(vertices[0].getX(), vertices[0].getY());
			calculaCentro();
			tempGrau *= -1;
			rotacao(tempGrau);
			criaArestas();
		}

	}

	/**
	 * Formata um n�mero double para aparecer apenas duas casas decimais e o
	 * transforma em String.
	 * 
	 * @param numero
	 *            N�mero a ser formatado.
	 * @return String com o n�mero formatado.
	 */
	public String formataNumero(double numero) {
		String retorno = "";
		DecimalFormat formatter = new DecimalFormat("0.00");
		try {
			retorno = formatter.format(numero);
		} catch (Exception ex) {
			System.err.println("Erro ao formatar numero: " + ex);
		}
		return retorno;
	}

	/**
	 * Retorna uma String com o conjunto de v�rtices da figura.
	 * 
	 * @return String com o conjunto de v�rtices da figura.
	 */
	public String strVertices() {
		String str = "V�rtices\n";
		for (int i = 0; i < vertices.length; i++) {
			str += "(" + formataNumero(vertices[i].getX()) + " , ";
			str += formataNumero(vertices[i].getY()) + ")\n";
		}
		return str;
	}

	/**
	 * Retorna String com o conjunto de arestas da figura.
	 * 
	 * @return String com o conjunto de arestas da figura.
	 */
	public String strArestas() {
		String str = "Arestas\n";
		for (int i = 0; i < arestas.length; i++) {
			str += "(" + formataNumero(arestas[i].getV1().getX()) + " , ";
			str += formataNumero(arestas[i].getV1().getY()) + ") - (";
			str += formataNumero(arestas[i].getV2().getX()) + " , ";
			str += formataNumero(arestas[i].getV2().getY()) + ")\n";
		}
		return str;
	}

	/**
	 * Retorna String com as cores da borda e do preenchimento da figura.
	 * 
	 * @return String com as cores da borda e do preenchimento da figura.
	 */
	public String strCores() {
		String str = "";
		str += "Cor da Borda: ";
		if (getCorBorda() == Color.black) {
			str += "Preto\n";
		} else if (getCorBorda() == Color.blue) {
			str += "Azul\n";
		} else if (getCorBorda() == Color.red) {
			str += "Vermelho\n";
		} else if (getCorBorda() == Color.pink) {
			str += "Rosa\n";
		} else if (getCorBorda() == Color.yellow) {
			str += "Amarelo\n";
		} else if (getCorBorda() == Color.green) {
			str += "Verde\n";
		}
		str += "Cor do Preenchimento: ";
		if (getCorPreenchimento() == Color.black) {
			str += "Preto\n";
		} else if (getCorPreenchimento() == Color.blue) {
			str += "Azul\n";
		} else if (getCorPreenchimento() == Color.red) {
			str += "Vermelho\n";
		} else if (getCorPreenchimento() == Color.pink) {
			str += "Rosa\n";
		} else if (getCorPreenchimento() == Color.yellow) {
			str += "Amarelo\n";
		} else if (getCorPreenchimento() == Color.green) {
			str += "Verde\n";
		}
		return str;
	}

	/**
	 * Retorna String com todos os dados da figura (V�rtices, Arestas, Cores da
	 * Borda e do Preenchimento e �rea e Per�metro).
	 * 
	 * @return String com todos os dados da figura
	 */
	public String strCompleto() {
		String str = "Dados da Figura: \n";
		str += strVertices();
		str += strArestas();
		str += strCores();
		str += "�rea\n";
		str += formataNumero(calculaArea()) + "\n";
		str += "Per�metro\n";
		str += formataNumero(calculaPerimetro()) + "\n";
		return str;
	}

	/**
	 * Calcula a �rea da figura
	 * 
	 * @return �rea da figura.
	 */
	public abstract double calculaArea();

	/**
	 * Calcula o per�metro da figura.
	 * 
	 * @return Per�metro da figura.
	 */
	public abstract double calculaPerimetro();

	/**
	 * Cria o conjunto de v�rtices da figura.
	 * 
	 * @param x
	 *            Valor x inicial.
	 * @param y
	 *            Valor y inicial.
	 */
	public abstract void criaVertices(double x, double y);

	/**
	 * Calcula o centro da figura.
	 * 
	 */
	public abstract void calculaCentro();

}

/**
 * Classe Pentagono
 * 
 * @author Eric
 *
 */
public class Pentagono extends Figura {
	/**
	 * Construtor da classe Pentagono.
	 * 
	 * @param x
	 *            Valor do x inicial
	 * @param y
	 *            Valor do y inicial
	 * @param lado
	 *            Tamanho do lado
	 */
	public Pentagono(double x, double y, double lado) {
		super(5, lado);
		criaVertices(x, y);
		criaArestas();
		calculaCentro();
	}

	/**
	 * Cria o conjunto de v�rtices da figura.
	 * 
	 * @param x
	 *            Valor x inicial.
	 * @param y
	 *            Valor y inicial.
	 */
	@Override
	public void criaVertices(double x, double y) {
		double temp1 = x + lado;
		double temp2 = y;
		vertices[0] = new Vertice(x, y);
		vertices[1] = new Vertice(temp1, temp2);
		temp1 += lado * 0.31;
		temp2 += lado * 0.95;
		vertices[2] = new Vertice(temp1, temp2);
		temp1 -= lado * 0.81;
		temp2 += lado * 0.59;
		vertices[3] = new Vertice(temp1, temp2);
		temp1 -= lado * 0.81;
		temp2 -= lado * 0.59;
		vertices[4] = new Vertice(temp1, temp2);
	}

	/**
	 * Calcula a ap�tema do pent�gono.
	 * 
	 * @return Ap�tema do pent�gono.
	 */
	public double calculaApotema() {
		double area = calculaArea();
		double perimetro = calculaPerimetro();
		double apotema = (2 * area) / perimetro;
		return apotema;
	}

	/**
	 * Calcula o centro da figura.
	 * 
	 */
	@Override
	public void calculaCentro() {
		double apotema = calculaApotema();
		double x1 = vertices[0].getX();
		double x2 = vertices[1].getX();
		double xCentro = (x1 + x2) / 2;
		double yCentro = vertices[0].getY() + apotema;
		Vertice centro = new Vertice(xCentro, yCentro);
		this.centro = centro;
	}

	/**
	 * Calcula a �rea da figura
	 * 
	 * @return �rea da figura.
	 */
	@Override
	public double calculaArea() {
		return 1.72048 * (lado * lado);
	}

	/**
	 * Calcula o per�metro da figura.
	 * 
	 * @return Per�metro da figura.
	 */
	@Override
	public double calculaPerimetro() {
		return lado * 5;
	}

}
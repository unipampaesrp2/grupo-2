/**
 * Classe Aresta
 * 
 * @author Diovane Freitas, Eric Oliveira, Matheus Le�o, Ricardo Gentile
 *
 */
public class Aresta {

	private Vertice v1, v2;

	/**
	 * Construtor da classe Aresta.
	 * 
	 * @param v1
	 *            Primeira v�rtice
	 * @param v2
	 *            Segunda v�rtice
	 */
	public Aresta(Vertice v1, Vertice v2) {
		this.v1 = v1;
		this.v2 = v2;
	}

	/**
	 * Retorna a primeira v�rtice da aresta.
	 * 
	 * @return Primeira v�rtice.
	 */
	public Vertice getV1() {
		return v1;
	}

	/**
	 * Atribui a primeira v�rtice da aresta.
	 * 
	 * @param v1
	 *            Primeira v�rtice.
	 */
	public void setV1(Vertice v1) {
		this.v1 = v1;
	}

	/**
	 * Retorna a segunda v�rtice da aresta.
	 * 
	 * @return Segunda v�rtice.
	 */
	public Vertice getV2() {
		return v2;
	}

	/**
	 * Atribui a segunda v�rtice da aresta.
	 * 
	 * @param v2
	 *            Segunda v�rtice.
	 */
	public void setV2(Vertice v2) {
		this.v2 = v2;
	}

}
